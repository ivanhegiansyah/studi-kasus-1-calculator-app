import { Switch } from "@headlessui/react";
import sun from "../../assets/Icon/sun.svg";
import moon from "../../assets/Icon/moon.svg";

const SwitchToggle = ({ enabled, setEnabled }) => {
  return (
    <Switch
      checked={enabled}
      onChange={setEnabled}
      className={`${
        enabled ? "bg-white" : "bg-btn-dark-gray-2"
      } relative inline-flex h-7 w-16 items-center rounded-full`}
    >
      <span className="sr-only">Enable notifications</span>
      <span>{enabled && <img className="ml-1" src={sun} alt="" />}</span>
      <span
        className={`transform transition ease-in-out duration-200 ${
          enabled
            ? "translate-x-3 bg-btn-light-gray"
            : "translate-x-1 bg-btn-dark-gray-1"
        } inline-block h-5 w-5 rounded-full`}
      />
      <span>{!enabled && <img className="ml-4" src={moon} alt="" />}</span>
    </Switch>
  );
};

export default SwitchToggle;
